# Welcome!

This project features a website built from a Docker image and deployed via GitLab CI/CD. The Docker image is built, then it is uploaded to the Container Registry, and the container scanning pulls the built image and conducts scans, then the application is published to GitLab Pages.

You can access the site [here](https://gl-demo-ultimate-smorris.gitlab.io/docker-app-example/).

Code derived from this [tutorial](https://docker-curriculum.com/).